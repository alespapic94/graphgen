# GraphGen #

This repository contains a graph generator to generate random graphs or graphs where the edge between two nodes exists with a certain probability. It is developed for [ALGator](https://github.com/ALGatorDevel/Algator) project to build large test sets.

### How to use ###

java GraphGen [options...]

 -F VAL : The testset name  
 -G VAL : Type of graph (GN - random graph, GNP - random with edge probabilities) (default: GN)  
 -f VAL : The output folder for the results  
 -g N   : Number of graphs (default: 1)  
 -h     : Print help (default: true)  
 -k N   : Increase of vertices in each graph (default: 10)  
 -n N   : Number of vertices in a graph (default: 10)  
 -p N   : Probability of an edge existing between two nodes. REQUIRED FOR GNP GRAPHS (default: 0.5)  
 -s N   : Seed to repeat graph generation (default: 42)  

##### Example #####

java -jar "GraphGen.jar" -G GNP -s 42 -n 10 -g 100 -p 0.08 -f ~/Desktop/ -F testset1

### Requirements ###

* [alg4 library](https://github.com/kevin-wayne/algs4)

### License ###

This code is released under GPLv3.