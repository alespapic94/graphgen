/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ap.graphgen.utils;

import java.util.HashSet;
import java.util.Iterator;

/**
 *
 * @author alespapic
 */
public class Functions {
    
    public static int getFirst(HashSet set) {
        Iterator iterator = set.iterator();
        while (iterator.hasNext()) {
          return (int) iterator.next();
        }
        return -1;
    }
}
