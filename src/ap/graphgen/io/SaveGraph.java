/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ap.graphgen.io;

import ap.graphgen.random.AbstractGraphGenerator;
import edu.princeton.cs.algs4.Edge;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author alespapic
 */

public class SaveGraph {
    public static void graphToFile(String outFolder, String graphType, AbstractGraphGenerator generator) {
        refToFile(outFolder, graphType, generator);
        String outputFilename = String.format("%s/%dEWG_%s.txt", outFolder, generator.V(), graphType);
        try (Writer w = new FileWriter(outputFilename)) {
            w.write(String.format("%d\n", generator.V()));
            w.write(String.format("%d\n", generator.E()));
            for (Edge e : generator.get()) {
                w.write(String.format("%d %d %2f\n", e.either(), e.other(e.either()), e.weight()));
            }
            w.flush();
        } catch (IOException ex) {
            Logger.getLogger(SaveGraph.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private static void refToFile(String outFolder, String graphType, AbstractGraphGenerator generator) {
        String outputFilename = String.format("%s/%dEWG_%s_refsol.txt", outFolder, generator.V(), graphType);
        try (Writer w = new FileWriter(outputFilename)) {
            w.write(String.format("%f\n", generator.refSolution()));
            w.flush();
        } catch (IOException ex) {
            Logger.getLogger(SaveGraph.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void listAllToFile(String outFolder, String projectName, int counter, String graphType, AbstractGraphGenerator generator) {
        String outputFilename = String.format("%s/%s.txt", outFolder, projectName);
        try (Writer w = new FileWriter(outputFilename, true)) {
            int N = generator.V();
            w.append(String.format("test%d:%d:FILE:%s/%dEWG_%s.txt:%s/%dEWG_%s_refsol.txt\n", 
                    counter, N, projectName, N, graphType, projectName, N, graphType));
            w.flush();
        } catch (IOException ex) {
            Logger.getLogger(SaveGraph.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}