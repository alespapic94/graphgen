/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ap.graphgen.random;

import ap.graphgen.utils.Functions;
import edu.princeton.cs.algs4.CC;
import java.util.Random;
import edu.princeton.cs.algs4.Edge;
import edu.princeton.cs.algs4.EdgeWeightedGraph;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

/**
 * 
 * @author alespapic
 */

// Returns a random graph on n nodes. Each edge is inserted independently with probability p.
public class RandomGNP extends AbstractGraphGenerator {
    
    private final double p;
    
    public RandomGNP(int n, double p) {
        this.n = n;
        this.p = p;
        this.r = new Random();
        this.adj = new ArrayList<>();
        this.ewg = new EdgeWeightedGraph(this.n);
        this.e = 0;
        this.run();
    }
    
    public RandomGNP(int n, double p, long seed) {
        this.n = n;
        this.p = p;
        this.seed = seed;
        this.r = new Random(this.seed);
        this.adj = new ArrayList<>();
        this.ewg = new EdgeWeightedGraph(this.n);
        this.e = 0;
        this.run();
    }
    
    @Override
    protected final void run() {
        // Generate edge weighted graph
        for (int i = 0; i < this.n; i++) {
            for (int j = i + 1; j < this.n; j++) {
                if (this.r.nextDouble() < this.p) {
                    Edge tmpE = new Edge(i, j, this.r.nextDouble());
                    this.adj.add(tmpE);
                    this.ewg.addEdge(tmpE);
                    this.e++;
                }
            }
        }
        // Check if generated graph is single connected component else correct it
        CC components = new CC(this.ewg);
        if (components.count() > 1) {
            // Extract a list of components and vertices in it
            HashMap<Integer, HashSet<Integer>> ccset = new HashMap<>(components.count());
            ArrayList<Integer> keys = new ArrayList<>();
            for (int i = 0; i < this.n; i++) {
                int cid = components.id(i);
                if (!ccset.containsKey(cid)) {
                    ccset.put(cid, new HashSet<>());
                    keys.add(cid);
                }
                ccset.get(cid).add(i);
            }
            do {
                // Randomly connect components
                int ccid1 = this.r.nextInt(keys.size());
                int ccid2 = this.r.nextInt(keys.size());
                if (ccid1 == ccid2) {
                    continue;
                }
                HashSet<Integer> tmp1 = ccset.get(keys.get(ccid1));
                HashSet<Integer> tmp2 = ccset.get(keys.get(ccid2));
                // Make an edge to connect two components
                int v = Functions.getFirst(tmp1);
                int u = Functions.getFirst(tmp2);
                Edge tmpE = new Edge(v, u, this.r.nextDouble());
                this.adj.add(tmpE);
                this.ewg.addEdge(tmpE);
                this.e++;
                // Merge connected components
                ccset.remove(keys.get(ccid2));
                keys.remove(ccid2);
                tmp1.addAll(tmp2);
            } while(ccset.size() > 1);
        }
        components = new CC(this.ewg);
        if (components.count() == 1) {
            this.refResult();
        }
    }
}
