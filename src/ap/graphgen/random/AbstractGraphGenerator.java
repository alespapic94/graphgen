/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ap.graphgen.random;

import edu.princeton.cs.algs4.Edge;
import edu.princeton.cs.algs4.EdgeWeightedGraph;
import edu.princeton.cs.algs4.LazyPrimMST;
import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author alespapic
 */
public abstract class AbstractGraphGenerator {
    
    protected int n;
    protected int e;
    protected long seed;
    protected double optimal;
    protected Random r;
    protected ArrayList<Edge> adj;
    protected EdgeWeightedGraph ewg;
    
    public ArrayList<Edge> get() {
        return this.adj;
    }
    
    public EdgeWeightedGraph G() {
        return this.ewg;
    }
    
    public int V() {
        return this.n;
    }
    
    public int E() {
        return this.e;
    }
    
    public double refSolution() {
        return this.optimal;
    }
    
    protected abstract void run();
    
    protected void refResult() {
        this.optimal = 0.0;
        LazyPrimMST lpmst = new LazyPrimMST(this.ewg);
        for (Edge edge : lpmst.edges()) {
            this.optimal += edge.weight();
        }
    }
    
    @Override
    public String toString() {
        return Integer.toString((int)this.seed);
    }
}
