/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ap.graphgen.main;

import java.nio.file.Files;
import ap.graphgen.io.SaveGraph;
import ap.graphgen.random.AbstractGraphGenerator;
import ap.graphgen.random.RandomGN;
import ap.graphgen.random.RandomGNP;
import java.io.File;
import java.nio.file.Paths;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;
import org.kohsuke.args4j.OptionHandlerFilter;
/**
 *
 * @author alespapic
 */
public class GraphGen {

    @Option(name = "-f", required = true, usage = "The output folder for the results")
    private String outFolder = "";
    @Option(name = "-F", required = true, usage = "The testset name")
    private String testsetName = "";
    @Option(name = "-G", required = false, usage = "Type of graph (GN - random graph, GNP - random with edge probabilities)")
    private String graphType = "GN";
    @Option(name = "-s", required = false, usage = "Seed to repeat graph generation")
    private long seed = 42;
    @Option(name = "-n", required = false, usage = "Number of vertices in a graph")
    private int vertices = 10;
    @Option(name = "-p", required = false, usage = "Probability of an edge existing between two nodes. REQUIRED FOR GNP GRAPHS")
    private double probability = 0.5;
    @Option(name = "-g", required = false, usage = "Number of graphs")
    private int graphs = 1;
    @Option(name = "-k", required = false, usage = "Increase of vertices in each graph")
    private int step = 10;
    @Option(name="-h", usage="Print help")
    private boolean help = false;
    
    public static void main(String[] args) {
        new GraphGen().run(args);
    }
    
    private void parseArgs(String[] args) {
        CmdLineParser parser = new CmdLineParser(this);
        try {
            parser.parseArgument(args);   
            if (help) {
                parser.printUsage(System.err);
                System.err.println();
                System.err.println("Example: java GraphGen" + parser.printExample(OptionHandlerFilter.ALL));
                System.exit(0);
            }
        } catch(CmdLineException e) {
            System.err.println(e.getMessage());
            System.err.println("java GraphGen [options...]");
            System.err.println();
            parser.printUsage(System.err);
            System.err.println();
            System.err.println("Example: java GraphGen" + parser.printExample(OptionHandlerFilter.PUBLIC));
            System.exit(1);
        }
    }
    
    private AbstractGraphGenerator generateGraph() {
        if (this.graphType.equals("GN")) {
            RandomGN rgn = new RandomGN(this.vertices, this.seed);
            return rgn;
        } else if (this.graphType.equals("GNP")) {
            RandomGNP rgnp = new RandomGNP(this.vertices, this.probability, this.seed);
            return rgnp;
        }
        return null;
    }
    
    public void run(String[] args) {
        // Parse inputs
        parseArgs(args);
        String outDir = String.format("%s/%s", outFolder, testsetName);
        // Generate the graphs
        for (int i = 0; i < this.graphs; i++, this.vertices += this.step) {
            AbstractGraphGenerator gen = generateGraph();
            // Save graph to file
            if (!Files.isDirectory(Paths.get(outDir))) {
                File f = new File(outDir);
                f.mkdirs();
            }
            SaveGraph.graphToFile(outDir, graphType, gen);
            SaveGraph.listAllToFile(outFolder, testsetName, i + 1, graphType, gen);
        }
    }
}
